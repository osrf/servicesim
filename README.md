# REPOSITORY MOVED

## This repository has moved to

https://github.com/osrf/servicesim

## Issues and pull requests are backed up at

https://osrf-migration.github.io/osrf-projects-gh-pages/#!/osrf/servicesim

## Until BitBucket removes Mercurial support, this read-only mercurial repository will be at

https://bitbucket.org/osrf-migrated/servicesim

## More info at

https://community.gazebosim.org/t/important-gazebo-and-ignition-are-going-to-github/533

